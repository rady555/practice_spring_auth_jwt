package com.example.todolist.controller;

import com.example.todolist.jwt.JwtTokenUtil;
import com.example.todolist.model.Entity.MyUser;
import com.example.todolist.model.Entity.UserDto;
import com.example.todolist.model.request.UserRequest;
import com.example.todolist.model.request.UserRequestToken;
import com.example.todolist.model.response.ResponseMessages;
import com.example.todolist.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;


@RestController
@RequestMapping("/auth")
public class AuthorController {
    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;

    public AuthorController(UserService userService, AuthenticationManager authenticate, JwtTokenUtil jwtTokenUtil ) {
        this.userService = userService;
        this.authenticationManager = authenticate;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @PostMapping("/register")
    public ResponseEntity<?> insertUser(@RequestBody UserRequest userRequest){
        UserDto userDto = userService.insertUser(userRequest);
        ResponseMessages<UserDto> response = ResponseMessages.<UserDto>builder()
                .payload(userDto)
                .localDateTime(LocalDateTime.now())
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }
    @PostMapping("/login")
//    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest jwtRequest, @RequestBody UserRequest userRequest) throws Exception {
//        authenticate(jwtRequest.getEmail(), jwtRequest.getPassword());
//        final UserDetails userDetails = userService
//                .loadUserByUsername(jwtRequest.getEmail());
//        final String token = jwtTokenUtil.generateToken(userDetails);
//
//        return ResponseEntity.ok(new JwtResponse(token));
        public ResponseEntity<?> logIn(@RequestBody UserRequest userRequest) throws Exception {
        authenticate(userRequest.getUserEmail(), userRequest.getUserPassword());
        final UserDetails userDetails = userService
                .loadUserByUsername(userRequest.getUserEmail());
        System.out.println(userDetails);
            String jwt = jwtTokenUtil.generateToken(userDetails);
            ResponseMessages responseMessages = new ResponseMessages();
            UserRequestToken userRequestToken = new UserRequestToken();
            UserDto user = userService.selectUser(userRequest.getUserEmail());
        System.out.println("Email" + userRequest.getUserEmail());
            userRequestToken.setUserId(user.getUserId());
            userRequestToken.setEmail(userRequest.getUserEmail());
            userRequestToken.setToken(jwt);
            responseMessages.setPayload(userRequestToken);
            responseMessages.setLocalDateTime(LocalDateTime.now());
            responseMessages.setSuccess(true);
            return ResponseEntity.ok().body(responseMessages);
    }


    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
//@PostMapping("/login")
//public ResponseEntity<?> createAuthenticationToken(@RequestBody UserRequest userRequest) throws Exception {
//    authenticate(userRequest.getUserEmail(), userRequest.getUserPassword());
//    UserRequestToken authenticationResponse = new UserRequestToken();
//    final UserDetails userDetails = userService
//            .loadUserByUsername(userRequest.getUserEmail());
//    final String token = jwtTokenUtil.generateToken(userDetails);
//    authenticationResponse.setUserId(11);
//    authenticationResponse.setToken(token);
//    authenticationResponse.setEmail(userRequest.getUserEmail());
//    ResponseMessages responseMessages = new ResponseMessages();
//    responseMessages.setLocalDateTime(LocalDateTime.now());
//    responseMessages.setPayload(authenticationResponse);
//    responseMessages.setSuccess(true);
//    return ResponseEntity.ok().body(responseMessages);
//}
//
//    private void authenticate(String username, String password) throws Exception {
//        try {
//            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
//        } catch (DisabledException e) {
//            throw new Exception("USER_DISABLED", e);
//        } catch (BadCredentialsException e) {
//            throw new Exception("INVALID_CREDENTIALS", e);
//        }
//    }


}
