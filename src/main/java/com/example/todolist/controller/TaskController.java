package com.example.todolist.controller;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SecurityRequirement(name = "bearerAuth")
public class TaskController {
    @GetMapping("/user")
    public String getUser(){
        return "User";
    }
    @GetMapping("/admin")
    public String getAdmin(){
        return "admin";
    }

}
