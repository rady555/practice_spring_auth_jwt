package com.example.todolist.service;

import com.example.todolist.model.Entity.MyUser;
import com.example.todolist.model.Entity.UserDto;
import com.example.todolist.model.request.UserRequest;
import com.example.todolist.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {
   private final UserRepository userRepository;
   private final PasswordEncoder passwordEncoder;
   private final ModelMapper mapper;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, ModelMapper mapper) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.mapper = mapper;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.findUserByEmail(email);
    }

    public UserDto insertUser(UserRequest userRequest) {
//        User user = userRepository.insertUser(userRequest);
//        UserDto userDto = new UserDto();
//        userDto.setUserId(user.getUserId());
//        userDto.setUserEmail(user.getUserEmail());
//        userDto.setUserRole(user.getUserRole());
        userRequest.setUserPassword(passwordEncoder.encode(userRequest.getUserPassword()));
        MyUser myUser = userRepository.insertUser(userRequest);
        return mapper.map(myUser,UserDto.class);
    }

    public UserDto selectUser(String email) {
        MyUser myUser = userRepository.findUserByEmail(email);
        return mapper.map(myUser,UserDto.class);
    }


//    public User getUserByEmail(UserRequest userRequest){
//
//    }
}
