package com.example.todolist.repository;


import com.example.todolist.model.Entity.MyUser;
import com.example.todolist.model.request.UserRequest;
import org.apache.ibatis.annotations.*;

@Mapper
public interface UserRepository {
    @Select("""
               INSERT INTO user_tb
               (email,password,role)
               VALUES(#{myUser.userEmail}, #{myUser.userPassword},'ROLE_USER')
               RETURNING *
            """)
    @Results(id="mapUser", value = {
            @Result(property = "userId", column = "id"),
            @Result(property = "userEmail", column = "email"),
            @Result(property = "userRole", column = "role"),
            @Result(property = "userPassword", column = "password")
    })
    MyUser insertUser(@Param("myUser") UserRequest userRequest);
    @Select(""" 
            SELECT * FROM user_tb
            WHERE email = #{email}
            """)
    @ResultMap("mapUser")
    MyUser findUserByEmail(String email);
    @Select(""" 
            SELECT * FROM user_tb
            WHERE email = #{userEmail}
            """)
    @ResultMap("mapUser")
    MyUser selectUser(UserRequest userRequest);



}
