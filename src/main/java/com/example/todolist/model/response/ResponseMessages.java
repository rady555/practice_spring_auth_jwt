package com.example.todolist.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseMessages<T> {
    private T payload;
    private LocalDateTime localDateTime;
    private boolean success;
}
