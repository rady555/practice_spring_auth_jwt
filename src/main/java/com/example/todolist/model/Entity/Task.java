package com.example.todolist.model.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Task {
    Integer taskId;
    String taskName;
    String taskDescription;
    LocalDateTime taskDate;
    Boolean status;
    Integer userId;
    Integer categoryId;
}
